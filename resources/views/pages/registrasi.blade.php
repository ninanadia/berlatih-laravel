@extends('layout.master')
@section('judul')
Registrasi
@endsection
@section('content')
    <form action="/kirim" method="post" >
        @csrf
        <label>First Name : </label><br>
        <input type="text" id="fname" name="first_name"><br><br>
        <label>Last Name : </label><br>
        <input type="text" id="lname" name="last_name"><br><br>
        <label>Gender</label><br><br>
        <input type="radio" id="male" name="Male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="Female">
        <label for="female">Female</label><br><br>
        <label for="nationality">Nationality</label><br><br>
        <select name="nationality" id="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="amerika">Amerika</option>
            <option value="inggris">Inggris</option>
        </select>
        <br><br>
        <label>Language Spoken</label><br><br>
        <input type="checkbox" name="bahasa1" id="bahasa1">
        <label for="bahasa1"> Bahasa Indonesia</label><br>
        <input type="checkbox" name="bahasa2" id="bahasa2">
        <label for="bahasa2"> English</label><br>
        <input type="checkbox" name="bahasa3" id="bahasa3">
        <label for="bahasa3"> Other</label><br><br>
        <label for="bio">Bio</label><br>
        <textarea  name="bio" rows="10" cols="30"></textarea>
        <br><br>
        <input type="submit" value="Sign Up">

    </form>
    @endsection
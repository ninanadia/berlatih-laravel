<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function pendaftaran(){
        return view('pages.registrasi');
    }

    public function welcome(Request $request){
        //dd($request->all());
        return view('pages.welcome');
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/data-tables', function(){
    return view('pages.data-table');
});

Route::get('/table', function(){
    return view('pages.table');
});

Route::get('/', 'HomeController@dashboard');
Route::get('/register', 'AuthController@pendaftaran');
Route::post('/kirim', 'AuthController@welcome');

//CRUD CAST
//mengarah ke form create data
Route::get('/cast/create', 'CastController@create');
//menyimpan data baru ke tabel Cast
Route::post('/cast', 'CastController@store');
//menampilkan list data cast
Route::get('/cast', 'CastController@index');
//detail data
Route::get('/cast/{cast_id}', 'CastController@show');
//mengarah ke form edit data
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
//update data
Route::put('/cast/{cast_id}', 'CastController@update');
//delete data
Route::delete('/cast/{cast_id}', 'CastController@destroy');
